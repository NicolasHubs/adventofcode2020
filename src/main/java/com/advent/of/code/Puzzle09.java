package com.advent.of.code;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("DuplicatedCode")
public class Puzzle09 extends AbstractPuzzle {
    public Puzzle09(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 9;
    }

    @Override
    public String solvePart1() {
        int preamble = 25;
        List<Long> numbers = Arrays.stream(getPuzzleInput().split("\n")).map(Long::parseLong)
                .collect(Collectors.toList());
        List<Long> rangedSum;
        long result = 0L;
        for (int i = preamble; i < numbers.size(); i++) {
            rangedSum = new ArrayList<>();
            for (int j = i - preamble; j < i; j++) {
                for (int k = j + 1; k < i; k++) {
                    rangedSum.add(numbers.get(j) + numbers.get(k));
                }
            }
            if (!rangedSum.contains(numbers.get(i))) {
                result = numbers.get(i);
            }
        }
        return String.valueOf(result);
    }

    @Override
    public String solvePart2() {
        List<Long> numbers = Arrays.stream(getPuzzleInput().split("\n")).map(Long::parseLong)
                .collect(Collectors.toList());
        long result = 0L;
        long part1Result = Long.parseLong(solvePart1());
        int indexOfPart1 = 0;

        for (Long nb : numbers) {
            if (nb == part1Result) {
                break;
            }
            indexOfPart1++;
        }

        for (int i = 0; i < indexOfPart1 - 2; i++) {
            for (int j = i + 2; j < indexOfPart1; j++) {
                long sum = sumNumbers(i, j, numbers);
                if (sum == part1Result) {
                    List<Long> finalResult = new ArrayList<>();
                    for (int index = i; index < j; index++) {
                        finalResult.add(numbers.get(index));
                    }
                    finalResult = finalResult.stream().sorted().collect(Collectors.toList());
                    return String.valueOf(finalResult.get(0) + finalResult.get(finalResult.size() - 1));
                }
            }
        }
        return String.valueOf(result);
    }

    private long sumNumbers(int start, int end, List<Long> numbers) {
        long total = 0L;
        for (int i = start; i < end; i++) {
            total += numbers.get(i);
        }
        return total;
    }
}
