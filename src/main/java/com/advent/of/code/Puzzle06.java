package com.advent.of.code;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("DuplicatedCode")
public class Puzzle06 extends AbstractPuzzle {
    public Puzzle06(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 6;
    }

    @Override
    public String solvePart1() {
        int count = 0;
        List<String> lines = Arrays.stream(getPuzzleInput().split("\n\n")).map(line -> line.replace("\n", " ").replace(" ", "")).collect(Collectors.toList());
        for (String line : lines) {
            Set<Integer> questions = new HashSet<>();
            count += line.chars().filter(c -> {
                if (!questions.contains(c)) {
                    questions.add(c);
                    return true;
                } else {
                    return false;
                }
            }).count();
        }
        return String.valueOf(count);
    }

    @Override
    public String solvePart2() {
        int count = 0;
        int groupSize = 0;
        Map<Character, Integer> groupAnswers = generateAnswerMap();
        List<String> lines = Arrays.stream(getPuzzleInput().replace("\n\n", "\n;\n").split("\n")).collect(Collectors.toList());
        for (String line : lines) {
            if (line.equals(";")) {
                for (Map.Entry<Character, Integer> elem : groupAnswers.entrySet()) {
                    if (elem.getValue() == groupSize) {
                        count++;
                    }
                }
                groupAnswers = generateAnswerMap();
                groupSize = 0;
            } else {
                groupSize++;
                for (char letter : line.toCharArray()) {
                    groupAnswers.merge(letter, 1, Integer::sum);
                }
            }
        }
        for (Map.Entry<Character, Integer> elem : groupAnswers.entrySet()) {
            if (elem.getValue() == groupSize) {
                count++;
            }
        }
        return String.valueOf(count);
    }

    private Map<Character, Integer> generateAnswerMap() {
        Map<Character, Integer> groupAnswers = new HashMap<>();
        for (char letter = 'a'; letter <= 'z'; letter++) {
            groupAnswers.put(letter, 0);
        }
        return groupAnswers;
    }

}
