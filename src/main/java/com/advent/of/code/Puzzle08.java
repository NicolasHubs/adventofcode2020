package com.advent.of.code;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("DuplicatedCode")
public class Puzzle08 extends AbstractPuzzle {
    public Puzzle08(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 8;
    }

    int accumulator = 0;

    @Override
    public String solvePart1() {
        List<Instruction> program = new ArrayList<>();
        for (String line : getPuzzleInput().split("\n")) {
            String[] splittedLine = line.split(" ");
            Operation op = Operation.valueOf(splittedLine[0].toUpperCase());
            int val;
            String valStr = splittedLine[1];
            if (valStr.charAt(0) == '+') {
                valStr = valStr.substring(1);
            }
            val = Integer.parseInt(valStr);
            if (op == Operation.JMP)
                val--;
            program.add(new Instruction(op, val));
        }
        isProgramValid(program);
        return String.valueOf(accumulator);
    }

    private boolean isProgramValid(List<Instruction> program) {
        boolean isValid = true;
        for (int i = 0; i < program.size(); i++) {
            Instruction ins = program.get(i);
            if (ins.execution()) {
                switch (ins.op) {
                    case ACC:
                        accumulator += ins.val;
                        break;
                    case JMP:
                        i += ins.val;
                        break;
                    case NOP:
                    default:
                }
            } else {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    private enum Operation {
        ACC("acc"),
        NOP("nop"),
        JMP("JMP");
        private final String name;

        Operation(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }

    private static class Instruction {
        Operation op;
        int val;
        boolean isExecuted;

        public Instruction(Operation op, int val) {
            this.op = op;
            this.val = val;
            isExecuted = false;
        }

        public Operation getOp() {
            return op;
        }

        public int getVal() {
            return val;
        }

        public boolean execution() {
            if (isExecuted) {
                return false;
            } else {
                isExecuted = true;
                return true;
            }
        }
    }


    @Override
    public String solvePart2() {
        List<Instruction> mainProgram = new ArrayList<>();
        List<Integer> indexToModify = new ArrayList<>();
        int indexLine = 0;
        for (String line : getPuzzleInput().split("\n")) {
            String[] splittedLine = line.split(" ");
            Operation op = Operation.valueOf(splittedLine[0].toUpperCase());
            int val;
            String valStr = splittedLine[1];
            if (valStr.charAt(0) == '+') {
                valStr = valStr.substring(1);
            }
            val = Integer.parseInt(valStr);
            if (op == Operation.JMP) {
                val--;
                indexToModify.add(indexLine);
            } else if (op == Operation.NOP) {
                indexToModify.add(indexLine);
            }
            mainProgram.add(new Instruction(op, val));
            indexLine++;
        }

        int index = 0;
        List<Instruction> modifiedProgram = new ArrayList<>(mainProgram);
        while (!isProgramValid(modifiedProgram)) {
            accumulator = 0;
            modifiedProgram = cloneList(mainProgram);
            Instruction ins = modifiedProgram.get(indexToModify.get(index));
            Operation op = ins.op;
            if (op == Operation.NOP) {
                op = Operation.JMP;
            } else if (op == Operation.JMP) {
                op = Operation.NOP;
            }
            ins.op = op;
            index++;
        }

        return String.valueOf(accumulator);
    }

    private static List<Instruction> cloneList(List<Instruction> original) {
        List<Instruction> clone = new ArrayList<>(original.size());
        for (Instruction ins : original) clone.add(new Instruction(ins.op, ins.val));
        return clone;
    }
}
