package com.advent.of.code;

@SuppressWarnings("DuplicatedCode")
public class Puzzle03 extends AbstractPuzzle {
    public Puzzle03(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 3;
    }

    private boolean[][] initForest(String[] forestLines, int width, int height) {
        boolean[][] forest = new boolean[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                forest[i][j] = (forestLines[j].charAt(i) == '#');
            }
        }
        return forest;
    }

    private int calculateEncounteredTrees(int moveX, int moveY, boolean[][] forest, int forestHeight, int forestWidth) {
        int currPosX = 0;
        int currPosY = 0;
        int totalTrees = 0;
        while (currPosY < forestHeight) {
            if (forest[currPosX % forestWidth][currPosY]) {
                totalTrees++;
            }
            currPosX += moveX;
            currPosY += moveY;
        }
        return totalTrees;
    }

    @Override
    public String solvePart1() {
        String[] forestLines = getPuzzleInput().split("\n");
        int moveX = 3;
        int moveY = 1;
        int forestHeight = forestLines.length;
        int forestWidth = forestLines[0].length();
        boolean[][] forest = initForest(forestLines, forestWidth, forestHeight);
        return String.valueOf(calculateEncounteredTrees(moveX, moveY, forest, forestHeight, forestWidth));
    }

    @Override
    public String solvePart2() {
        String[] forestLines = getPuzzleInput().split("\n");
        int forestHeight = forestLines.length;
        int forestWidth = forestLines[0].length();
        boolean[][] forest = initForest(forestLines, forestWidth, forestHeight);
        long result = 1;
        result *= calculateEncounteredTrees(1, 1, forest, forestHeight, forestWidth);
        result *= calculateEncounteredTrees(3, 1, forest, forestHeight, forestWidth);
        result *= calculateEncounteredTrees(5, 1, forest, forestHeight, forestWidth);
        result *= calculateEncounteredTrees(7, 1, forest, forestHeight, forestWidth);
        result *= calculateEncounteredTrees(1, 2, forest, forestHeight, forestWidth);
        return String.valueOf(result);
    }
}
