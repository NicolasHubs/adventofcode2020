package com.advent.of.code;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings("DuplicatedCode")
public class Puzzle04 extends AbstractPuzzle {
    public Puzzle04(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 4;
    }

    @Override
    public String solvePart1() {
        List<String> requiredFields = Arrays.asList("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid");
        int validPassport = 0;
        Map<String, String> passport;
        List<String> lines = Arrays.stream(getPuzzleInput().split("\n\n")).map(line -> line.replace("\n", " ")).collect(Collectors.toList());
        for (String line : lines) {
            passport = new HashMap<>();
            fillPassport(passport, line.split(" "));
            if (isPasseportValidPart1(passport, requiredFields)) {
                validPassport++;
            }
        }
        return String.valueOf(validPassport);
    }

    private void fillPassport(Map<String, String> passport, String[] fields) {
        for (String field : fields) {
            String[] keyVal = field.split(":");
            passport.put(keyVal[0], keyVal[1]);
        }
    }

    private boolean isPasseportValidPart1(Map<String, String> passport, List<String> requiredFields) {
        for (String requiredField : requiredFields) {
            if (passport.get(requiredField) == null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String solvePart2() {
        Map<String, Pattern> requiredFields = new HashMap<>();
        requiredFields.put("byr", Pattern.compile("^(19[2-9][0-9]|200[0-2])$"));
        requiredFields.put("iyr", Pattern.compile("^(20(1[0-9]|20))$"));
        requiredFields.put("eyr", Pattern.compile("^(20(2[0-9]|30))$"));
        requiredFields.put("hgt", Pattern.compile("^(1([5-8][0-9]|9[0-3])cm|([5-6][0-9]|7[0-6])in)$"));
        requiredFields.put("hcl", Pattern.compile("^#([0-9]|[a-f]){6}$"));
        requiredFields.put("ecl", Pattern.compile("^(amb|blu|brn|gry|grn|hzl|oth)$"));
        requiredFields.put("pid", Pattern.compile("^([0-9]){9}$"));
        int validPassport = 0;
        Map<String, String> passport;
        List<String> lines = Arrays.stream(getPuzzleInput().split("\n\n")).map(line -> line.replace("\n", " ")).collect(Collectors.toList());
        for (String line : lines) {
            passport = new HashMap<>();
            fillPassport(passport, line.split(" "));
            if (isPasseportValidPart2(passport, requiredFields)) {
                validPassport++;
            }
        }
        return String.valueOf(validPassport);
    }

    private boolean isPasseportValidPart2(Map<String, String> passport, Map<String, Pattern> requiredFields) {
        for (Map.Entry<String, Pattern> requiredField : requiredFields.entrySet()) {
            Pattern regex = requiredField.getValue();
            String fieldToValidate = passport.get(requiredField.getKey());
            if (fieldToValidate == null || !regex.matcher(fieldToValidate).matches()) {
                return false;
            }
        }
        return true;
    }

}
