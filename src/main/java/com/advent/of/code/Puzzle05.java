package com.advent.of.code;

import org.apache.commons.lang3.tuple.Pair;

import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("DuplicatedCode")
public class Puzzle05 extends AbstractPuzzle {
    public Puzzle05(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 5;
    }

    @Override
    public String solvePart1() {
        int max = 0;
        for (String seatString : getPuzzleInput().split("\n")) {
            Pair<Integer, Integer> row = Pair.of(0, 127);
            for (int i = 0; i < 7; i++) {
                char step = seatString.charAt(i);
                row = processRange(step, row.getLeft(), row.getRight());
            }
            Pair<Integer, Integer> col = Pair.of(0, 7);
            for (int i = 7; i < 10; i++) {
                char step = seatString.charAt(i);
                col = processRange(step, col.getLeft(), col.getRight());
            }
            max = Math.max(max, (row.getLeft() * 8 + col.getLeft()));

        }
        return String.valueOf(max);
    }

    private Pair<Integer, Integer> processRange(char step, int from, int to) {
        int sum = (from + to);
        if (step == 'F' || step == 'L') {
            to = sum / 2;
        } else {
            from = (int) Math.ceil(((double) sum) / 2);

        }
        return Pair.of(from, to);
    }

    @Override
    public String solvePart2() {
        int max = 0;
        Set<Integer> ids = new HashSet<>();
        for (String seatString : getPuzzleInput().split("\n")) {
            Pair<Integer, Integer> row = Pair.of(0, 127);
            for (int i = 0; i < 7; i++) {
                char step = seatString.charAt(i);
                row = processRange(step, row.getLeft(), row.getRight());
            }
            Pair<Integer, Integer> col = Pair.of(0, 7);
            for (int i = 7; i < 10; i++) {
                char step = seatString.charAt(i);
                col = processRange(step, col.getLeft(), col.getRight());
            }
            int res = (row.getLeft() * 8 + col.getLeft());
            ids.add(res);
            max = Math.max(max, res);
        }
        for (int i = 0; i < max; i++) {
            if (!ids.contains(i) && ids.contains(i + 1) && ids.contains(i - 1)) {
                return String.valueOf(i);
            }
        }
        return "error";
    }
}
