package com.advent.of.code;

import java.util.Arrays;
import java.util.List;

public class Main {
    private static final PuzzleInputFetcher PUZZLE_INPUT_FETCHER = new PuzzleInputFetcher();
    private static final List<AbstractPuzzle> PUZZLES = Arrays.asList(
            new Puzzle01(PUZZLE_INPUT_FETCHER.getPuzzleInput(1)),
            new Puzzle02(PUZZLE_INPUT_FETCHER.getPuzzleInput(2)),
            new Puzzle03(PUZZLE_INPUT_FETCHER.getPuzzleInput(3)),
            new Puzzle04(PUZZLE_INPUT_FETCHER.getPuzzleInput(4)),
            new Puzzle05(PUZZLE_INPUT_FETCHER.getPuzzleInput(5)),
            new Puzzle06(PUZZLE_INPUT_FETCHER.getPuzzleInput(6)),
            new Puzzle07(PUZZLE_INPUT_FETCHER.getPuzzleInput(7)),
            new Puzzle08(PUZZLE_INPUT_FETCHER.getPuzzleInput(8)),
            new Puzzle09(PUZZLE_INPUT_FETCHER.getPuzzleInput(9)),
            new Puzzle10(PUZZLE_INPUT_FETCHER.getPuzzleInput(10))
    );

    public static void main(String[] args) {
        PUZZLES.forEach(puzzle -> {
            String day = String.format("%02d", puzzle.getDay());
            System.out.println("Day " + day + " Part 1: " + puzzle.solvePart1());
            System.out.println("Day " + day + " Part 2: " + puzzle.solvePart2());
        });
    }
}
