package com.advent.of.code;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("DuplicatedCode")
public class Puzzle10 extends AbstractPuzzle {
    public Puzzle10(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 10;
    }

    @Override
    public String solvePart1() {
        List<Integer> numbers = Arrays.stream(getPuzzleInput().split("\n")).map(Integer::parseInt).sorted().collect(Collectors.toList());
        numbers.add(numbers.stream().max(Comparator.comparingInt(Integer::intValue)).orElse(0) + 3);
        int outlet = 0;
        List<Integer> d1 = new ArrayList<>();
        List<Integer> d3 = new ArrayList<>();
        for (int n : numbers) {
            if (n - outlet == 1) {
                outlet = n;
                d1.add(n);
            } else if (n - outlet == 3) {
                outlet = n;
                d3.add(n);
            }
        }
        return String.valueOf(d1.size() * d3.size());
    }

    @Override
    public String solvePart2() {
        return "";
    }
}
