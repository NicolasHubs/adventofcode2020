package com.advent.of.code;

import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings("DuplicatedCode")
public class Puzzle07 extends AbstractPuzzle {
    public Puzzle07(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 7;
    }

    @Override
    public String solvePart1() {
        Pattern firstBagPattern = Pattern.compile("^(.+) bags contain (.+)\\.$");
        Pattern childBagsPattern = Pattern.compile("(\\d+?) (.+?) bag[s]?");
        List<BagPart1> root = new ArrayList<>();
        String[] lines = getPuzzleInput().split("\n");
        Map<String, BagPart1> cachedBags = initCachedBagsPart1(Arrays.stream(lines).map(elem -> elem.split(" bags contain")[0]).collect(Collectors.toList()));
        for (String bagLine : lines) {
            Matcher matcher = firstBagPattern.matcher(bagLine);
            if (matcher.matches()) {
                String mainBagColor = matcher.group(1);
                BagPart1 bag = cachedBags.get(mainBagColor);
                String childBags = matcher.group(2);
                if (childBags.equals("no other bags")) {
                    root.add(bag);
                    continue;
                }
                for (String childBagLine : childBags.split(", ")) {
                    matcher = childBagsPattern.matcher(childBagLine);
                    if (matcher.matches()) {
                        String childBagColor = matcher.group(2);
                        BagPart1 childBag = cachedBags.get(childBagColor);
                        bag.insertBag(childBag);
                    } else {
                        System.err.println("ERROR-CHILD -> " + childBagLine);
                        throw new IllegalStateException("Unhandled case 2");
                    }
                }
                root.add(bag);
            } else {
                System.err.println("ERROR -> " + bagLine);
                throw new IllegalStateException("Unhandled case");
            }
        }
        return String.valueOf(root.stream().filter(elem -> !elem.color.equals("shiny gold") && elem.containsBagColor("shiny gold")).count());
    }

    private Map<String, BagPart1> initCachedBagsPart1(List<String> bagsColors) {
        Map<String, BagPart1> bags = new HashMap<>();
        for (String color : bagsColors) {
            BagPart1 bag = new BagPart1(color);
            bags.put(color, bag);
        }
        return bags;
    }

    private Map<String, BagPart2> initCachedBagsPart2(List<String> bagsColors) {
        Map<String, BagPart2> bags = new HashMap<>();
        for (String color : bagsColors) {
            BagPart2 bag = new BagPart2(color);
            bags.put(color, bag);
        }
        return bags;
    }

    @Override
    public String solvePart2() {
        Pattern firstBagPattern = Pattern.compile("^(.+) bags contain (.+)\\.$");
        Pattern childBagsPattern = Pattern.compile("(\\d+?) (.+?) bag[s]?");
        List<BagPart2> root = new ArrayList<>();
        String[] lines = getPuzzleInput().split("\n");
        Map<String, BagPart2> cachedBags = initCachedBagsPart2(Arrays.stream(lines).map(elem -> elem.split(" bags contain")[0]).collect(Collectors.toList()));
        for (String bagLine : lines) {
            Matcher matcher = firstBagPattern.matcher(bagLine);
            if (matcher.matches()) {
                String mainBagColor = matcher.group(1);
                BagPart2 bag = cachedBags.get(mainBagColor);
                String childBags = matcher.group(2);
                if (childBags.equals("no other bags")) {
                    root.add(bag);
                    continue;
                }
                for (String childBagLine : childBags.split(", ")) {
                    matcher = childBagsPattern.matcher(childBagLine);
                    if (matcher.matches()) {
                        String childBagColor = matcher.group(2);
                        BagPart2 childBag = cachedBags.get(childBagColor);
                        int childBagNumber = Integer.parseInt(matcher.group(1));
                        bag.insertBag(Pair.of(childBagNumber, childBag));
                    } else {
                        System.err.println("ERROR-CHILD -> " + childBagLine);
                        throw new IllegalStateException("Unhandled case 2");
                    }
                }
                root.add(bag);
            } else {
                System.err.println("ERROR -> " + bagLine);
                throw new IllegalStateException("Unhandled case");
            }
        }
        return String.valueOf(cachedBags.get("shiny gold").requiredBagSum());
    }

    private static class BagPart1 {
        String color;
        List<BagPart1> bags = new ArrayList<>();

        public BagPart1(String color) {
            this.color = color;
        }

        public void insertBag(BagPart1 bag) {
            this.bags.add(bag);
        }

        public boolean containsBagColor(String color) {
            for (BagPart1 bag : bags) {
                if (bag.containsBagColor(color)) {
                    return true;
                }
            }
            return this.color.equals(color);
        }
    }

    private static class BagPart2 {
        String color;
        List<Pair<Integer, BagPart2>> bags = new ArrayList<>();

        public BagPart2(String color) {
            this.color = color;
        }

        public void insertBag(Pair<Integer, BagPart2> bag) {
            this.bags.add(bag);
        }

        public int requiredBagSum() {
            int sum = 0;
            for (Pair<Integer, BagPart2> bag : bags) {
                int childValue = bag.getLeft();
                int childSum = bag.getRight().requiredBagSum();
                sum += childValue + childValue * childSum;
            }
            return ((bags.isEmpty()) ? 0 : sum);
        }
    }
}
