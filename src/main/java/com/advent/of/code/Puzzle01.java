package com.advent.of.code;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings("DuplicatedCode")
public class Puzzle01 extends AbstractPuzzle {
    public Puzzle01(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 1;
    }

    @Override
    public String solvePart1() {
        long objective = 2020;
        List<Long> numbersList = Arrays.stream(getPuzzleInput().split("\n")).map(Long::parseLong).sorted().collect(Collectors.toList());
        int leftInd = 0;
        int rightInd = numbersList.size() - 1;

        while (leftInd < rightInd) {
            long leftNum = numbersList.get(leftInd);
            long rightNum = numbersList.get(rightInd);
            long sum = leftNum + rightNum;
            if (sum == objective) {
                return String.valueOf(leftNum * rightNum);
            } else if (sum < objective) {
                leftInd++;
            } else {
                rightInd--;
            }
        }
        throw new IllegalStateException("Unable to find the solution");
    }

    @Override
    public String solvePart2() {
        long objective = 2020;
        List<Long> numbers = Arrays.stream(getPuzzleInput().split("\n")).map(Long::parseLong).sorted().collect(Collectors.toList());
        for (int i = 0; i < numbers.size() - 2; i++) {
            long leftNum = numbers.get(i);
            int centerInd = i + 1;
            int rightInd = numbers.size() - 1;
            while (centerInd < rightInd) {
                long centerNum = numbers.get(centerInd);
                long rightNum = numbers.get(rightInd);
                long sum = leftNum + centerNum + rightNum;
                if (sum == objective) {
                    return String.valueOf(leftNum * centerNum * rightNum);
                } else if (sum < objective) {
                    centerInd++;
                } else {
                    rightInd--;
                }
            }
        }
        throw new IllegalStateException("Unable to find the solution");
    }

    @SuppressWarnings("unused")
    public String anotherPart1() {
        Map<Long, Long> numbers = Arrays.stream(getPuzzleInput().split("\n"))
                .map(Long::parseLong)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        for (long n1 : numbers.keySet()) {
            long n2 = 2020 - n1;
            if (numbers.containsKey(n2)) {
                if (n1 != n2) {
                    return String.valueOf(n1 * n2);
                } else if (numbers.get(n2) >= 2) { // Gestion des duplicats
                    return String.valueOf(n1 * n2);
                }
            }
        }

        throw new IllegalStateException("Unable to find the solution");
    }


}
