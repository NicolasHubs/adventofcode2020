package com.advent.of.code;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("DuplicatedCode")
public class Puzzle02 extends AbstractPuzzle {
    public Puzzle02(String puzzleInput) {
        super(puzzleInput);
    }

    @Override
    public int getDay() {
        return 2;
    }

    @Override
    public String solvePart1() {
        int nbCorrect = 0;
        for (String line : getPuzzleInput().split("\n")) {
            String[] splittedLine = line.split(" ");
            String[] splittedRange = splittedLine[0].split("-");
            Pair<Integer, Integer> range = Pair.of(Integer.valueOf(splittedRange[0]), Integer.valueOf(splittedRange[1]));
            char letter = splittedLine[1].replace(":", "").charAt(0);
            String sentence = splittedLine[2];

            long occur = sentence.chars().filter(c -> c == letter).count();


            if (occur >= range.getLeft() && occur <= range.getRight())
                nbCorrect++;
        }
        return String.valueOf(nbCorrect);
    }

    @Override
    public String solvePart2() {
        int nbCorrect = 0;
        for (String line : getPuzzleInput().split("\n")) {
            String[] splittedLine = line.split(" ");
            String[] splittedRange = splittedLine[0].split("-");
            Pair<Integer, Integer> range = Pair.of(Integer.parseInt(splittedRange[0]) - 1, Integer.parseInt(splittedRange[1]) - 1);
            char letter = splittedLine[1].replace(":", "").charAt(0);
            String sentence = splittedLine[2];
            boolean firstCondition = sentence.charAt(range.getLeft()) == letter;
            boolean secondCondition = range.getRight() < sentence.length() && sentence.charAt(range.getRight()) == letter;
            if (firstCondition && !secondCondition || !firstCondition && secondCondition) {
                nbCorrect++;
            }
        }
        return String.valueOf(nbCorrect);
    }
}
